package com.elugo.smack.model

/**
 * Created by elugo.
 * Date: 12/abr/2018.
 */
class Channel (val name:String, val description:String, val id:String){

    override fun toString(): String {
        return "#$name"
    }
}