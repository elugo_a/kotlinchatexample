package com.elugo.smack.controller

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.elugo.smack.R
import com.elugo.smack.services.AuthService
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginSpinner.visibility=View.INVISIBLE
    }

    fun loginLoginBtnClicked(view: View) {
        hideKeyboard()
        enableSpinner(true)
        val email = loginEmailTxt.text.toString().trim()
        val pwd = loginPasswordTxt.text.toString().trim()

        if (email.isEmpty() || pwd.isEmpty()) {
            showMessage("Please make sure email and password are filled in.")
        } else {
            AuthService.loginUser(email, pwd) { loginSuccess ->
                if (loginSuccess) {
                    AuthService.findUserByEmail(this) { findSuccess ->
                        if (findSuccess) {
                            finish()
                        } else {
                            errorToast()
                        }
                    }
                } else {
                    errorToast()
                }
            }
        }
    }

    fun loginCreateUserBtnClicked(view: View) {
        val intent = Intent(this, CreateUserActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        enableSpinner(false)
    }

    fun errorToast() {
        showMessage("Something went wrong, please try again.")
        enableSpinner(false)
    }

    fun enableSpinner(enable:Boolean) {
        if(enable) {
            loginSpinner.visibility = View.VISIBLE
        } else {
            loginSpinner.visibility = View.INVISIBLE
        }
        loginLoginBtn.isEnabled = !enable
        loginCreateUserBtn.isEnabled = !enable
    }

    private fun hideKeyboard() {
        val inputMgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMgr.isAcceptingText) {
            inputMgr.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }
}
