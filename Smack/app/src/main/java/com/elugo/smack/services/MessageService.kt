package com.elugo.smack.services

import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.elugo.smack.controller.App
import com.elugo.smack.model.Channel
import com.elugo.smack.model.Message
import com.elugo.smack.utilities.URL_GET_CHANNELS
import com.elugo.smack.utilities.URL_GET_MESSAGES
import org.json.JSONException

/**
 * Created by elugo.
 * Date: 12/abr/2018.
 */
object MessageService {

    val channels = ArrayList<Channel>()
    val messages = ArrayList<Message>()

    fun getChannels(complete: (Boolean) -> Unit) {
        val channelsRequest = object : JsonArrayRequest(Request.Method.GET, URL_GET_CHANNELS, null, Response.Listener { response ->

            try {
                channels.clear()
                for (x in 0 until response.length()) {
                    val channel = response.getJSONObject(x)
                    val channelName = channel.getString("name")
                    val channelDesc = channel.getString("description")
                    val channelId = channel.getString("_id")

                    val newChannel = Channel(channelName, channelDesc, channelId)

                    channels.add(newChannel)
                }
                complete(true)
            } catch (e: JSONException) {
                Log.d("ERROR", "EXC: ${e.localizedMessage}")
                complete(false)
            }
        }, Response.ErrorListener { error ->
            Log.d("ERROR", "Could not retrieve channels")
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", "Bearer ${App.prefs.authToken}")
                return headers
            }
        }

        App.prefs.requestQueue.add(channelsRequest)
    }

    fun getMessages(channelId: String, complete: (Boolean) -> Unit) {
        val url = "$URL_GET_MESSAGES$channelId"

        val messagesRequest = object : JsonArrayRequest(Request.Method.GET, url, null, Response.Listener {response->
            clearMessages()
            try {
                for (x in 0 until response.length()) {
                    val msg = response.getJSONObject(x)
                    val msgBody = msg.getString("messageBody")
                    val channelId = msg.getString("channelId")
                    val id = msg.getString("_id")
                    val userName = msg.getString("userName")
                    val userAvatar = msg.getString("userAvatar")
                    val userAvatarColor = msg.getString("userAvatarColor")
                    val timeStamp = msg.getString("timeStamp")

                    val newMessage = Message(msgBody, userName, channelId, userAvatar, userAvatarColor, id, timeStamp)
                    this.messages.add(newMessage)
                }
                complete(true)
            } catch (e: JSONException) {
                Log.d("ERROR", "EXC: ${e.localizedMessage}")
                complete(false)
            }
        }, Response.ErrorListener {
            Log.d("ERROR", "Could not retrieve messages")
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", "Bearer ${App.prefs.authToken}")
                return headers
            }
        }

        App.prefs.requestQueue.add(messagesRequest)
    }

    fun clearMessages() {
        messages.clear()
    }

    fun clearChannels() {
        channels.clear()
    }
}