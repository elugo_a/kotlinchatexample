package com.elugo.smack.model

/**
 * Created by elugo.
 * Date: 15/abr/2018.
 */
class Message constructor(val message: String, val userName: String, val channelId: String,
                          val userAvatar: String, val userAvatarColor: String, val id: String,
                          val timeStamp: String) {
}