package com.elugo.smack.controller

import android.app.Application
import com.elugo.smack.utilities.SharedPrefs

/**
 * Created by elugo.
 * Date: 13/abr/2018.
 */
class App : Application() {

    companion object {
        lateinit var prefs: SharedPrefs
    }

    override fun onCreate() {
        prefs = SharedPrefs(applicationContext)
        super.onCreate()
    }
}